### 運作機制說明

![](d1.png)

backend主要是透過socket io來溝通.
透過openvinoHeadPose取得webcam的畫面轉換成base64的data透過image topic傳給前端網頁使用
並且透過angle topic傳送 pitch roll yew到後端去做查詢

angle_info啟動一個client. 當收到data時就去查詢,
比較方式是以angle_info給予的yaw去查詢`mongo`取得小於這個值最接近的N張圖的資訊.
把最接近的前三筆採用`Three Dimensional Distance Calculator`計算之後取的最近似值的圖.
然後使用painting這個topic將資訊傳送給前端網頁使用.
