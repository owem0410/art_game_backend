import socketio
import time

sio = socketio.Client()
@sio.event
def connect():
    print('connection established')

@sio.event
def my_message(data):
    print('message received with ', data)
    sio.emit(u'my response', {'response': 'my response'})
    sio.emit(u'angle', {'response': u'111'})

@sio.event
def disconnect():
    print('disconnected from server')

def main():
  sio.connect('http://192.168.31.130:3001')
  while True:
    my_message("hello")
    time.sleep(1)
  sio.wait()

main()
