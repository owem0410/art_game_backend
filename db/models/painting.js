const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
  id: {
    type: Number,
    unique: true,
  },
  name: String,
  country: String,
  start_year: String,
  end_year: String,
  museum: String,
  painter: String,
  url: String,
  filename: String,
  pitch: Number,
  yaw: Number,
  roll: Number,
})
