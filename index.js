const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const ioclient = require('socket.io-client');
const testFolder = './tests/';
const fs = require('fs');
const mg = require('./db/main');
const eucli = require('eucli');
const _ = require('lodash');
require('dotenv').config();

function getRandomInt() {
  return Math.floor(Math.random() * Math.floor(90));
}

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/query', (req, res) => {
  const yaw = req.query.yaw
  mg.Painting.
    find({yaw: {$lt: +yaw}}).
    sort({yaw: -1}).
    exec((err, o) => {
      return res.json({err, o, yaw: yaw, gi: getRandomInt()})
    })
});

io.on('connection', function (socket) {
  // forward data to frontend
  socket.on('image', (img) => {
    io.emit('image', img)
  })
  socket.on('angle', (d) => {
    io.emit('angle_info', {
      selectedPaw: d.data.split(",")[0],
      selectedRaw: d.data.split(",")[1],
      selectedYaw: d.data.split(",")[2],
    })
  })
})

soct = ioclient.connect(process.env["HOST_SERVE"])
soct.on('angle_info', (data) => {
  const selectedYaw = data.selectedYaw
  mg.Painting.
    find({yaw: {$lt: +selectedYaw}}).
    select("id name country star_year end_year url filename pitch yaw roll").
    sort({yaw: -1}).
    exec((err, records) => {
      if(err)
        console.error(err)
      records = _.take(records, 3)
      records = _.chain(records).
      map((d) => {
        let key = eucli(
          [data.selectedPaw, data.selectedRaw, data.selectedYaw],
          [d.pitch, d.roll, d.yaw],
        )
        return {sort_key: key, data: d}
      }).
      orderBy(['sort_key'],['asc'])
      .value();
      // console.log(_.map(records, (o) => o.sort_key))
      // console.log('records[0]', records[0].data)
      if(records !== undefined || !_.isEmpty(records) || records[0] !== undefined){
        try {
          const result = records[0].data
          io.emit('painting', {data: result, selectedYaw, inputdata: data});
        } catch {

        }
      }
    })
});

app.use(express.static('public'));
server.listen(process.env["WEB_PORT"]);
