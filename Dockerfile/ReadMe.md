### 啟動Demo群集.

#### Setp 1
* 擷取需要的git source codes
```
sh init.sh
```

#### Step 2
* 啟動docker-compose
```
docker-compose up -d
```
ps. opevinobase 編譯時間會比較長. 可以使用`build.sh`先行編譯

#### Step 3
* 群集啟動之後, 輸入csv和圖片資訊
```
docker exec -it dockerfile_web_1 bash
ruby data_import_on_docker.rb
exit
```

#### Step 4
* 設定及啟動openvino headpose detection
```
docker exec -it dockerfile_openvinohead_1 bash
cd root/openvino-HeadPose/HeadPose/
pip install -r requirement.txt
./run.sh
```

#### 啟動成功
可以開啟:
http://localhost:3001/index.html

#### 停止及刪除叢集
```
docker-compose stop
docker-compose rm -f
```
### 如果有新的web頁面, 如何更新web?
```
docker rmi artapp:v0.1
cd web/packages
./update_package.sh
cd ../../
# 然後再從Step 2執行一次
``
